//
//  MovieError.swift
//  MoviesTraktAPI
//
//  Created by elpaps on 10/8/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import Foundation

enum MovieError: Error {
  case parsing(description: String)
  case network(description: String)
}
