//
//  MovieFetcher.swift
//  MoviesTraktAPI
//
//  Created by elpaps on 10/8/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import Foundation
import Combine

protocol MoviesFetchable {
  func searchPopularMovies(
    forQuery query: String,andPage page:Int
  ) -> AnyPublisher<[MovieResponse], MovieError>

  func getImagePopularMovies(
    forId imdb: String
  ) -> AnyPublisher<Data, MovieError>
    
}


class MovieFetcher {
  private let session: URLSession
  init(session: URLSession = .shared) {
    self.session = session
  }
}


// MARK: - MoviesFetchable
extension MovieFetcher: MoviesFetchable {
  func getImagePopularMovies(forId imdb: String) -> AnyPublisher<Data, MovieError> {
    return request(with:getImageMoviesComponents(withId: imdb))
  }
    
  func searchPopularMovies(
    forQuery query: String,andPage page:Int
  ) -> AnyPublisher<[MovieResponse], MovieError> {
    return request(with: searhMoviesComponents(withQuery: query, andPage: page))
  }


  private func request<T>(
    with components: URLComponents
  ) -> AnyPublisher<T, MovieError> where T: Decodable {
    guard let url = components.url else {
      let error = MovieError.network(description: "Couldn't create URL")
      return Fail(error: error).eraseToAnyPublisher()
    }
    var request = URLRequest(url: url)
    request.addValue(TraktAPI.key, forHTTPHeaderField: "trakt-api-key")
    request.addValue("2", forHTTPHeaderField: "trakt-api-version")
    
    return session.dataTaskPublisher(for: request)
      .mapError { error in
        .network(description: error.localizedDescription)
      }
      .flatMap(maxPublishers: .max(1)) { pair in
        decode(pair.data)
      }
      .eraseToAnyPublisher()
  }
}

// MARK: - trakt API
private extension MovieFetcher {
  struct TraktAPI {
    static let scheme = "https"
    static let host = "api.trakt.tv"
    static let path = ""
    static let key = "81633b089d37da78787d99544c4b1056ba4c779cffbc27c495b36df1cea9bcca"
  }
  
  struct IMDBAPI {
    static let scheme = "https"
    static let host = "img.omdbapi.com"
    static let path = ""
    static let key = "f6317c02"
  }
    
    
  func searhMoviesComponents(
    withQuery query:String,andPage page:Int
   ) -> URLComponents {
     var components = URLComponents()
     components.scheme = TraktAPI.scheme
     components.host = TraktAPI.host
     components.path = TraktAPI.path + "/movies/popular"
     
     components.queryItems = [
       URLQueryItem(name: "query", value: query),
       URLQueryItem(name: "extended", value: "full"),
       URLQueryItem(name: "page", value: "\(page)"),
       URLQueryItem(name: "limit", value: "10")
     ]
     return components
   }
    
    func getImageMoviesComponents(
        withId imdb: String
    ) -> URLComponents {
        var components = URLComponents()
        components.scheme = IMDBAPI.scheme
        components.host = IMDBAPI.host
        components.path = IMDBAPI.path
        
        components.queryItems = [
            URLQueryItem(name: "i", value: imdb),
            URLQueryItem(name: "apikey", value: IMDBAPI.key),
        ]
        return components
    }
    
}
