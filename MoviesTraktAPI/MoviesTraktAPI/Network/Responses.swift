//
//  Responses.swift
//  MoviesTraktAPI
//
//  Created by elpaps on 10/8/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import Foundation

struct MovieResponse: Decodable {
    let title: String?
    let year: Int?
    let overview:String?
    let ids:IdReviews?
    
    struct IdReviews: Codable {
        let trakt: Int?
        let slug: String?
        let imdb: String?
        let tmdb: Int?
    }
}
