//
//  PopularViewModal.swift
//  MoviesTraktAPI
//
//  Created by elpaps on 10/9/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import Foundation

import SwiftUI
import Combine

class PopularMovieViewModel: ObservableObject {
  @Published var movie: String = "" {
    didSet {
        dataSource.removeAll()
        page = 1
    }
  }
  @Published var dataSource: [PopularMovieRowViewModel] = []

  private let movieFetcher: MoviesFetchable
  private var disposables = Set<AnyCancellable>()
  private var page = 0
    
    init(
        movieFetcher: MoviesFetchable,
        scheduler: DispatchQueue = DispatchQueue(label: "MovieViewModel")
    ) {
        self.movieFetcher = movieFetcher
        _ = $movie
            .dropFirst(1)
            .debounce(for: .seconds(0.5), scheduler: scheduler)
            .sink(receiveValue: fetchPopularMovies(forQuery:))
    }
    
    func getPopularMovies() {
        page += 1
        fetchPopularMovies(forQuery: movie)
    }

    func fetchPopularMovies(forQuery query: String="") {
        movieFetcher.searchPopularMovies(forQuery: query, andPage: page)
            .map { response in
                response.map(PopularMovieRowViewModel.init)
        }
        .receive(on: DispatchQueue.main)
        .sink(
            receiveCompletion: { [weak self] value in
                guard let self = self else { return }
                switch value {
                case .failure:
                    self.dataSource = []
                case .finished:
                    break
                }
            },
            receiveValue: { [weak self] movies in
                guard let self = self else { return }
                self.dataSource += movies
        })
            .store(in: &disposables)
    }

}
