//
//  PopularRowViewModel.swift
//  MoviesTraktAPI
//
//  Created by elpaps on 10/9/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import Foundation
import SwiftUI

struct PopularMovieRowViewModel:Identifiable {
    private let movie: MovieResponse
    
    init(movie: MovieResponse) {
        self.movie = movie
    }
    
    var id: String {
        return title + year + overview
    }
    
    var title: String {
        guard let title = movie.title else {return ""}
        return title
    }
    
    var year: String {
        guard let year = movie.year else {return ""}
        return "\(year)"
    }
    
    var overview: String {
        guard let overview = movie.overview else {return ""}
        return overview
    }
    
    var image: URL? {
        guard let id = movie.ids?.imdb else {return nil}
        return URL(string: "https://img.omdbapi.com/"+"?i=\(id)&apikey=f6317c02")
    }
}
