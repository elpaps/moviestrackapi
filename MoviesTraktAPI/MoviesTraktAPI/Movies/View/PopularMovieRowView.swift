//
//  PopularMovieRowView.swift
//  MoviesTraktAPI
//
//  Created by elpaps on 10/9/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import SwiftUI

struct PopularMovieRow: View {
  private let viewModel: PopularMovieRowViewModel
  
   init(viewModel: PopularMovieRowViewModel) {
        self.viewModel = viewModel
    }
  
    var body: some View {
        HStack {
            ImageViewWidget(imageUrl: viewModel.image)
            VStack(alignment: .leading) {
                Text("\(viewModel.title)" + " (\(viewModel.year))")
                    .font(.body)
                Text("\n\(viewModel.overview)")
                    .font(.caption)
            }
            .padding(.leading, 8)
        }
    }
}

struct ImageViewWidget:View {
    
    @ObservedObject var imageLoader: ImageLoader
    var imageUrl : URL?
       
    init(imageUrl: URL?,imageLoader:ImageLoader = ImageLoader()) {
        self.imageLoader = imageLoader
        self.imageUrl = imageUrl
    }
       
    var body: some View {
        Image(uiImage:imageLoader.image(for: imageUrl))
            .resizable()
            .frame(width: 70, height: 100)
            .clipShape(Rectangle())
            .overlay(Rectangle().stroke(Color.gray, lineWidth: 1))
    }
}
