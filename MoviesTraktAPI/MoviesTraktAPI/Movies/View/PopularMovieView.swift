//
//  PopularMovieView.swift
//  MoviesTraktAPI
//
//  Created by elpaps on 10/9/19.
//  Copyright © 2019 elpaps. All rights reserved.
//

import SwiftUI

struct PopularMovieView: View {
    @ObservedObject var viewModel: PopularMovieViewModel
    var body: some View {
      NavigationView {
        List {
          searchField
          if viewModel.dataSource.isEmpty {
            emptySection
          }else{
            moviesSection
          }
          Rectangle().foregroundColor(.clear).onAppear {
                self.viewModel.getPopularMovies()
          }
        }
        .listStyle(GroupedListStyle())
        .navigationBarTitle("Movies")
      }
    }
    
}

private extension PopularMovieView {
    var searchField: some View {
        HStack(alignment: .center) {
            TextField("Search Movie", text: $viewModel.movie)
        }
    }
    var moviesSection: some View {
      Section {
        ForEach(viewModel.dataSource, content: PopularMovieRow.init(viewModel:))
      }
    }
    var emptySection: some View {
        Section {
            Text("No results")
                .foregroundColor(.gray)
        }
    }
}

